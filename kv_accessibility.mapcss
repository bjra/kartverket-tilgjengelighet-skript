meta 
{
    title: "Kartverket Accessibility";
    description: "JOSM Map Paint Style";
    version: "2021-07-19";
    author: "Johan Wiklund;Bjørn L. Rapp";
	icon: "https://avatars1.githubusercontent.com/u/2970101?s=200&v=4";
}

canvas
{
	fill-color: #fff;
}

/* <SETTINGS> */

setting::dekke
{
  type: boolean;
  label: tr("Dekke");
  default: false;
}
setting::gatetype
{
  type: boolean;
  label: tr("Gatetype");
  default: true;
}
setting::fangstdato
{
  type: boolean;
  label: tr("Fangstdato");
  default: false;
}
setting::veitype
{
  type: boolean;
  label: tr("Veitype");
  default: true;
}
setting::veitype_label
{
  type: boolean;
  label: tr("Veitype skrives ut");
  default: false;
}
setting::nodeConnections
{
  type: boolean;
  label: tr("Nodekobling");
  default: false;
}


/* </SETTINGS> */

/* Felles egenskaper*/

way|z1-12
{
	width: 1;
	z-index: 1;
}
way|z12-17
{
	width: 2;
	z-index: 1;
}
way|z17-1000
{
	width: 5;
	z-index: 1;
}

way[dekketilstand=Jevnt][setting("dekke")]
{
	casing-color: #4dff1c;
}
way[dekketilstand=Ujevnt][setting("dekke")]
{
	casing-color: #ff371c;
}

way[setting("fangstdato")]
{
	width: 5;
}

way[eval(substring(tag("forstedatafangstdato"),0,4))=2008][setting("fangstdato")]
{
	color: #e6d1e9;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2009][setting("fangstdato")]
{
	color: #dabbde;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2010][setting("fangstdato")]
{
	color: #cda4d3;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2011][setting("fangstdato")]
{
	color: #c18dc9;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2012][setting("fangstdato")]
{
	color: #b576be;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2013][setting("fangstdato")]
{
	color: #a85fb3;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2014][setting("fangstdato")]
{
	color: #9c49a8;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2015][setting("fangstdato")]
{
	color: #8f329d;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2016][setting("fangstdato")]
{
	color: #831b92;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2017][setting("fangstdato")]
{
	color: #761883;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2018][setting("fangstdato")]
{
	color: #691675;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2019][setting("fangstdato")]
{
	color: #5c1366;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2020][setting("fangstdato")]
{
	color: #4f1058;

}
way[eval(substring(tag("forstedatafangstdato"),0,4))=2021][setting("fangstdato")]
{
	color: #8d0b39;
}

/* TettstedVei */

way[objtype=TettstedVei][setting("dekke")]
{	casing-width: 2;
	casing-opacity: 1;
	casing-dashes: 0;}
way[objtype=TettstedVei][!dekketilstand][setting("dekke")]
{
	casing-color: #ffe51c;
}


way[objtype=TettstedVei][dekke=Asfalt][setting("dekke")]
{
	color: #10a303;
}
way[objtype=TettstedVei][dekke=Betong][setting("dekke")]
{
	color: #3982ad;
}
way[objtype=TettstedVei][dekke=Brostein][setting("dekke")]
{
	color: #8b03a3;
}
way[objtype=TettstedVei][dekke=Grus][setting("dekke")]
{
	color: #39ad79;
}
way[objtype=TettstedVei][dekke=Skogsbunn][setting("dekke")]
{
	color: #ad6039;
}
way[objtype=TettstedVei][dekke=Stein][setting("dekke")]
{
	color: #66bda0;
}
way[objtype=TettstedVei][dekke=Tre][setting("dekke")]
{
	color: #b87a00;
}
way[objtype=TettstedVei][gatetype=Fortau][setting("gatetype")]
{
	color: #f5428d;
	font-size: 14;
}
way[objtype=TettstedVei][gatetype=Gangfelt][setting("gatetype")]
{
	color: #000;
	dashes: 5,5;
	dashes-background-color: #fff;
}
way[objtype=TettstedVei][gatetype=Gangvei][setting("gatetype")]
{
	color: #f5428d;
	dashes: 5,5;
}
way[objtype=TettstedVei][gatetype="Gangvei og sykkelvei"][setting("gatetype")]
{
	color: #f5428d;
	dashes: 5,5;
	dashes-background-color: #0009ab;
}
way[objtype=TettstedVei][gatetype="Sykkelvei"][setting("gatetype")]
{
	color: #0009ab;
	dashes: 5,5;
}
way[objtype=TettstedVei][gatetype="Sti"][setting("gatetype")]
{
	color: #e3b900;
	dashes: 5,5;
}
way[objtype=TettstedVei][gatetype="Gate"][setting("gatetype")]
{
	color: #ff4d00;
	
}
way[gatetype="Gågate"][setting("gatetype")],
way[gatetype="Gågate og torg"][setting("gatetype")],
way[gatetype="Torg"][setting("gatetype")]
{
	color: #00ff22;
}
way[gatetype="Annet"][setting("gatetype")]
{
	color: #4cb06d;
}


/* FriluftTurvei */

way[objtype=FriluftTurvei][setting("veitype_label")]
{
	text: veitype;
	font-size: 14;
	text-halo-radius: 1.4;
	text-halo-color: #707070;
}

way[objtype=FriluftTurvei][veitype="Asfaltert vei"][setting("veitype")]
{
    color: #ff4d00;
}
way[objtype=FriluftTurvei][veitype="Gangvei og sykkelvei"][setting("veitype")]
{
    color: #f5428d;
	dashes: 5,5;
	dashes-background-color: #0009ab;
}
way[objtype=FriluftTurvei][veitype=Grusvei][setting("veitype")]
{
    color: #e0ca80;
}
way[objtype=FriluftTurvei][veitype=Sti][setting("veitype")]
{
    color: #e3b900;
	dashes: 5,5;
}
way[objtype=FriluftTurvei][veitype=Traktorvei][setting("veitype")]
{
    color: #e0ca80;
	dashes: 7,5;
}
way[objtype=FriluftTurvei][veitype="Skiløype"][setting("veitype")]
{
    color: #bb31e1;
	dashes: 7,5;
}
way[objtype=FriluftTurvei][veitype=Gangbane][setting("veitype")]
{
    color: #f5428d;
}

/* Noder */

node
{
    symbol-shape: circle;
    symbol-stroke-color: #fff;
    symbol-fill-color: #555;
	symbol-size: 4;
}

way!:closed > node|z1-15:connection[setting("nodeConnections")],
way!:closed >[index=1] node|z1-15[setting("nodeConnections")],
way!:closed >[index=-1] node|z1-15[setting("nodeConnections")]
{
	symbol-size: 3;
	symbol-stroke-opacity: 0;
}
way!:closed > node|z15-18:connection[setting("nodeConnections")],
way!:closed >[index=1] node|z15-18[setting("nodeConnections")],
way!:closed >[index=-1] node|z15-18[setting("nodeConnections")]
{
	symbol-size: 7;
}
way!:closed > node|z18-20:connection[setting("nodeConnections")],
way!:closed >[index=1] node|z18-20[setting("nodeConnections")],
way!:closed >[index=-1] node|z18-20[setting("nodeConnections")]
{
	symbol-size: 11;
}
way!:closed > node|z20-1000:connection[setting("nodeConnections")],
way!:closed >[index=1] node|z20-1000[setting("nodeConnections")],
way!:closed >[index=-1] node|z20-1000[setting("nodeConnections")]
{
	symbol-size: 13;
}

way > node[setting("nodeConnections")]
{
    symbol-shape: circle;
    symbol-stroke-color: #fff;
    symbol-fill-color: #de1e09;
	z-index: 60;
}

way > node:connection[setting("nodeConnections")]
{
    symbol-shape: circle;
    symbol-stroke-color: #fff;
    symbol-fill-color: #00c936;
	z-index: 50;
}

way|z1-12[setting("nodeConnections")]
{
	width: 1;
	z-index: 1;
}
way|z12-17[setting("nodeConnections")]
{
	width: 1.5;
	z-index: 1;
}
way|z17-1000[setting("nodeConnections")]
{
	width: 2;
	z-index: 1;
}