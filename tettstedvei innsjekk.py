import datetime
import sqlalchemy
from sqlalchemy.engine.url import URL
from geoalchemy2 import Geometry, functions
import json
import operator
import argparse
from typing import Union

unik_id_navn = 'lokalid'
sql_tabell_navn = 'tettstedvei'
sql_skjema_navn = 'tilgjengelighettettsted'
oblig_egenskaper = ["objtype", "gatetype", "trapp"]
andre_egenskaper = ["byggingpagar", "kommune", "kommentar", "belysning", "varmekabel", "dekke", "dekketilstand",
                    "bredde", "stigning", "tverrfall", "moteplass", "ledelinje", "ledelinjekontrast",
                    "tilgjengvurderingrulleman", "tilgjengvurderingelrull", "tilgjengvurderingsyn"]
trappe_egenskaper = ["trappbredde", "rekkverk", "rekkverkhoydeovre", "rekkverkhoydenedre", "farefeltoverst",
                     "oppmerksamhetsfeltnederst", "trappenesekontrast", "trappkontrast"]
gangfelt_egenskaper = ["lyssignal", "lydsignal", "nedsenk1", "nedsenk2"]
alle_egenskaper = [*oblig_egenskaper, *andre_egenskaper, *trappe_egenskaper, *gangfelt_egenskaper]
opphav = "E0000_uu_bogkat"
til_srid = 32633
fra_srid = 4326


def kl_14_idag():
    idag = datetime.date.today()
    tidspunkt = datetime.time(14, 0)
    tidssone = datetime.timezone(datetime.timedelta(hours=2))
    return datetime.datetime.combine(idag, tidspunkt, tidssone)


def list_duplikater(itererbar):
    opptelling = {}
    for i, v in enumerate(itererbar):
        if v in opptelling and v is not None:
            opptelling[v].append(i)
        else:
            opptelling[v] = [i]

    for indekser in opptelling.values():
        if len(indekser) > 1:
            yield indekser


def iterer_egenskap(objekter, egenskap_navn):
    for objekt in objekter:
        yield få_egenskap(objekt, egenskap_navn)


def få_egenskap(objekt, egenskap_navn):
    return objekt.get("properties", {}).get(egenskap_navn)


def test_viktige_egenskaper(objekt):
    for oblig_egenskap in oblig_egenskaper:
        if not få_egenskap(objekt, oblig_egenskap):
            raise ValueError(f"Objekt med {unik_id_navn} {få_egenskap(objekt, unik_id_navn)} mangler {oblig_egenskap}")


def test_ikke_trapp(objekt):
    if få_egenskap(objekt, "trapp") != "Ja" and (
            ugyldige_egenskaper := set(trappe_egenskaper) & objekt['properties'].keys()):
        print(f"Objekt med {unik_id_navn} {få_egenskap(objekt, unik_id_navn)} er ikke en trapp, "
              f"men hadde egenskapene {ugyldige_egenskaper} som tilhører trapper.")
        for ugyldig_nøkkel in ugyldige_egenskaper:
            del objekt['properties'][ugyldig_nøkkel]


def test_ikke_gangfelt(objekt):
    if få_egenskap(objekt, "gatetype") != "Gangfelt" and (
            ugyldige_egenskaper := set(gangfelt_egenskaper) & objekt['properties'].keys()):
        print(f"Objekt med {unik_id_navn} {få_egenskap(objekt, unik_id_navn)} er ikke et gangfelt, "
              f"men hadde egenskapene {ugyldige_egenskaper} som tilhører gangfelt.")
        for ugyldig_nøkkel in ugyldige_egenskaper:
            del objekt['properties'][ugyldig_nøkkel]


def er_geomtritype_linje(objekt):
    return objekt['geometry']['type'] == 'LineString'


def innsjekk(db_url: Union[str, URL], skjema_navn: str, tabell_navn: str, geojson_fil: str):
    motor = sqlalchemy.create_engine(db_url)
    metadata = sqlalchemy.MetaData(schema=skjema_navn)

    with motor.connect() as tilkobling:
        tabell = sqlalchemy.Table(
            tabell_navn, metadata, autoload=True, autoload_with=tilkobling)

        utvalg = tilkobling.execute(
            sqlalchemy.select(
                (tabell.columns[unik_id_navn],)
            ))

    eksisterende_ider = set(unik_id for unik_id, in utvalg)

    with open(geojson_fil, encoding='utf-8') as fil:
        json_data = json.load(fil)

    objektsamling = list(filter(er_geomtritype_linje, json_data['features']))
    duplikater = list_duplikater(iterer_egenskap(objektsamling, unik_id_navn))

    for duplikat_indekser in duplikater:
        lengder = (len(objektsamling[i]["geometry"]['coordinates']) for i in duplikat_indekser)
        behold_id_indeks, _ = max(zip(duplikat_indekser, lengder), key=operator.itemgetter(1))
        for i in duplikat_indekser:
            if i != behold_id_indeks:
                objektsamling[i]['properties'][unik_id_navn] = None

    # validering
    for objekt in objektsamling:
        test_viktige_egenskaper(objekt)
        test_ikke_trapp(objekt)
        test_ikke_gangfelt(objekt)

    tidspunkt = kl_14_idag()

    upd_data = []
    ins_data = []
    slette_data = []

    for objekt in objektsamling:
        geometri = objekt["geometry"]
        verdier = {k: få_egenskap(objekt, k) for k in alle_egenskaper}
        verdier['json_geometri'] = json.dumps(geometri)
        unik_id = få_egenskap(objekt, unik_id_navn)
        slett = få_egenskap(objekt, 'slett') == 'Ja'
        if unik_id in eksisterende_ider:
            verdier['unik_id'] = unik_id
            if slett:
                slette_data.append(verdier)
            else:
                upd_data.append(verdier)
        else:
            if slett:
                raise Exception(
                    f"Vei med {unik_id_navn} {unik_id} er markert for sletting, men kunne ikke finne den i databasen")
            ins_data.append(verdier)

    sql_fra_geojson = sqlalchemy.func.ST_Transform(
        sqlalchemy.func.ST_SetSRID(
            sqlalchemy.func.ST_GeomFromGeoJSON(sqlalchemy.bindparam('json_geometri')), fra_srid), til_srid)

    ins = tabell.insert(
    ).values(
        geometri=sql_fra_geojson,
        segmentlengde=sqlalchemy.func.ST_Length(sql_fra_geojson),
        opphav=opphav,
        forstedatafangstdato=tidspunkt,
        datafangstdato=tidspunkt)

    upd = tabell.update(
    ).values(
        geometri=sql_fra_geojson,
        segmentlengde=sqlalchemy.func.ST_Length(sql_fra_geojson),
        datafangstdato=tidspunkt
    ).where(sqlalchemy.bindparam('unik_id') == tabell.columns[unik_id_navn])

    delete = tabell.delete().where(sqlalchemy.bindparam('unik_id') == tabell.columns[unik_id_navn])

    with motor.begin() as transaksjon:
        if upd_data:
            transaksjon.execute(upd, upd_data)
        if ins_data:
            transaksjon.execute(ins, ins_data)
        if slette_data:
            transaksjon.execute(delete, slette_data)
    print(f"{len(ins_data)} nye veier ble lagt til, {len(slette_data)} veier  ble slettet, og {len(upd_data)} veier "
          f"ble oppdatert i tabellen {skjema_navn}.{sql_tabell_navn}.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("geojson", type=str, help="GeoJSON utrekk")
    parser.add_argument("database", type=str, help=(
        "Postgres tilkoblingstreng. f.eks \"postgresql://brukernavn:passord@adresse:5432/gisdatabase\""))
    parser.add_argument("--skjema", type=str, default=sql_skjema_navn, help="Postgres skjema")
    parser.add_argument("--tabell", type=str, default=sql_tabell_navn, help="Postgres tabell")

    argumenter = parser.parse_args()
    innsjekk(argumenter.database, argumenter.skjema, argumenter.tabell, argumenter.geojson)
